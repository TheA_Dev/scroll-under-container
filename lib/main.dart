import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp(),
    ));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: DrawerPage(),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverList(
              delegate: SliverChildListDelegate([
            Stack(
              children: [
                //Search and drawer
                Container(
                    height: 250,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/bg.jpg"))),
                    child: Stack(
                      children: [
                        Container(
                          height: 60,
                          margin: EdgeInsets.only(top: 70, left: 20, right: 20),
                          padding: EdgeInsets.only(left: 10, right: 10),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  _globalKey.currentState.openDrawer();
                                },
                                icon: Icon(
                                  Icons.menu_open,
                                ),
                                iconSize: 30,
                                color: Colors.black,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: TextField(
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Search",
                                    hintStyle: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15),
                                  ),
                                ),
                              ),
                              Icon(
                                Icons.notifications,
                                size: 30,
                                color: Colors.black,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                //Scroll Horizontal
                Container(
                    margin: EdgeInsets.only(top: 200),
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        itemCount: Categ.length,
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                  left: 20,
                                ),
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        colors: [
                                          Colors.grey,
                                          Colors.blueGrey,
                                        ]),
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image:
                                            AssetImage(Categ[index]['img']))),
                                child: Padding(
                                  padding: EdgeInsets.only(top: 8, left: 5),
                                  child: Text(
                                    Categ[index]['name'],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ),
                            ],
                          );
                        })),
              ],
            ),
            Container(
              height: 250,
              margin: EdgeInsets.only(top: 10),
              width: MediaQuery.of(context).size.width,
              child: Swiper(
                physics: BouncingScrollPhysics(),
                itemCount: swip.length,
                autoplay: true,
                pagination: SwiperPagination(
                  margin: EdgeInsets.only(left: 350)
                ),
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            colors: [
                              Colors.grey,
                              Colors.blueGrey,
                            ]),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(swip[index]['img'])
                      ),
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only( left: 200,bottom: 50),
                        child: Text(
                          Categ[index]['name'],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ]))
        ],
      ),
    );
  }
}

final swip = [
  {
    "img": "assets/swiper/lee.png",
  },
  {
    "img": "assets/swiper/fs.png",
  },
  {
    "img": "assets/swiper/bt.png",
  },
  {
    "img": "assets/swiper/fd.jpg",
  },
];
final Categ = [
  {
    "img": "assets/hoo.png",
    "name": "SUPPLEMENT",
  },
  {
    "img": "assets/fashion.jpg",
    "name": "FASHION",
  },
  {
    "img": "assets/beauty.jpg",
    "name": "BEAUTY",
  },
  {
    "img": "assets/food.jpg",
    "name": "FOOD",
  },
];

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              child: Container(
            color: Colors.amber,
            child: Center(
              child: Text("Hello JKJ Peter"),
            ),
          )),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          ListTile(
            title: Text("Peter"),
            trailing: Text("Parker"),
          ),
          Divider(
            thickness: 1.50,
            color: Colors.amber,
            height: 5,
            endIndent: 20,
            indent: 20,
          ),
          DrawerHeader(
              child: Container(
                color: Colors.amber,
                child: Center(
                  child: Text("Hello JKJ Peter"),
                ),
              )),
        ],
      ),
    );
  }
}
